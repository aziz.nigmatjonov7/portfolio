import { Main } from 'components/UI/Main/Main'
import SEO from 'components/SEO'
export default function Home() {
  return (
    <>
      <SEO />
      <Main />
    </>
  )
}